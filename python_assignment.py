# -*- coding: utf-8 -*-
"""python_assignment

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/18l1qRddP_fTdaBf9jGWHaOUza553r2Wr
"""

def find_longest_word_with_length(input_string):
   
    words = input_string.split()

    longest_word = ""
    longest_length = 0

    
    for word in words:
        
        word = word.strip(".,!?'\"")

        if len(word) > longest_length:
    
            longest_word = word
            longest_length = len(word)

    longest_word_with_length = longest_word + str(longest_length)

    return longest_word_with_length

input_string = "The quick brown fox jumps over the lazy dog"
result = find_longest_word_with_length(input_string)
print("Longest word with length:", result)

def check_symmetrical_and_palindrome(input_string):
    
    input_string = input_string.lower().replace(" ", "")

    
    if len(input_string) % 2 == 0:
        
        half1 = input_string[:len(input_string)//2]
        half2 = input_string[len(input_string)//2:]
    else:
        
        half1 = input_string[:len(input_string)//2]
        half2 = input_string[(len(input_string)//2)+1:]

    if half1 == half2[::-1]:

        if input_string == input_string[::-1]:
            return "The string is symmetrical and palindrome."
        else:
            return "The string is symmetrical but not palindrome."
    else:
        return "The string is neither symmetrical nor palindrome."

input_string = "amaama"
result = check_symmetrical_and_palindrome(input_string)
print(result)

import string

def check_pangram(input_string):

    input_string = input_string.lower().replace(" ", "")
    input_string = input_string.translate(str.maketrans("", "", string.punctuation))

    
    unique_chars = set(input_string)

    if len(unique_chars) == 26:
        return "yes"
    else:
        return "no"


input_string = "The quick brown fox jumps over the lazy dog"
result = check_pangram(input_string)
print(result)

def sort_words(input_string):
  
    words = input_string.split("-")

  
    words.sort()

    sorted_words = "-".join(words)

    return sorted_words

input_string = "green-red-yellow-black-white"
result = sort_words(input_string)
print("Sorted words:", result)

def is_prime(n):
    
    if n <= 1:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True

def print_twin_primes(n):
    
    twin_primes = []
    for i in range(3, n + 1):
        
        if is_prime(i) and is_prime(i + 2):
            twin_primes.append((i, i + 2))
    return twin_primes


n = int(input("Enter the upper limit: "))
twin_primes = print_twin_primes(n)

# Print the twin primes
print("Twin primes till", n, ":")
for pair in twin_primes:
    print(pair[0], "and", pair[1])

def double_characters(input_string):
    # Function to double each character in the input 
    doubled_string = ""
    for char in input_string:
        doubled_string += char * 2
    return doubled_string


input_string1 = "now"
result1 = double_characters(input_string1)
print("Doubled string for '{}': {}".format(input_string1, result1))

input_string2 = "123a!"
result2 = double_characters(input_string2)
print("Doubled string for '{}': {}".format(input_string2, result2))

def print_fibonacci_series(n):
   
    if n <= 0:
        print("Invalid input. Please provide a positive integer greater than 0.")
        return

    
    f1, f2 = 0, 1
    print("Fibonacci series:")
    print(f1, end=", ")
    for i in range(2, n):
        
        fn = f1 + f2
        print(fn, end=", ")
       
        f1, f2 = f2, fn
    print()

# Accept user input for the number to print
n = int(input("Enter the number of terms to print in the Fibonacci series: "))
print_fibonacci_series(n)

def count_odd_even_digits(number):
    
    odd_count = 0
    even_count = 0

   
    number_str = str(number)
    for char in number_str:
        
        digit = int(char)
        if digit % 2 == 0:
            
            even_count += 1
        else:
            
            odd_count += 1

    return odd_count, even_count


number = int(input("Enter a number: "))
odd_count, even_count = count_odd_even_digits(number)
print("Number of odd digits: ", odd_count)
print("Number of even digits: ", even_count)



def is_perfect_number(number):
    # Function to check if a given number is a perfect number
    if number <= 0:
        print("Invalid input. Please provide a positive integer greater than 0.")
        return False

    divisors_sum = 0
   
    for i in range(1, number):
        if number % i == 0:
           
            divisors_sum += i

    
    if divisors_sum == number:
        return True
    else:
        return False

# Accept user input for the number
number = int(input("Enter a number: "))
if is_perfect_number(number):
    print(number, "is a perfect number.")
else:
    print(number, "is not a perfect number.")

def print_even_length_words(string):
   
    words = string.split()  

    print("Even length words in the string:")
    for word in words:
        if len(word) % 2 == 0:
            print(word)


string = input("Enter a string: ")
print_even_length_words(string)

def print_pascals_triangle(n):
   
    if n <= 0:
        print("Invalid input. Please provide a positive integer greater than 0.")
        return

    triangle = [[1]]  irst r
    for i in range(1, n):
        prev_row = triangle[i - 1]
        new_row = [1]  

       
        for j in range(1, i):
            new_row.append(prev_row[j - 1] + prev_row[j])
        new_row.append(1)  # Last element of each row is always 1

        triangle.append(new_row) 
    # Print Pascal's triangle
    print("Pascal's Triangle (first", n, "rows):")
    for row in triangle:
        print(" ".join(map(str, row))) 


n = int(input("Enter the number of rows of Pascal's triangle to print: "))
print_pascals_triangle(n)

def calculate_sum(input_str):

    input_str = input_str.strip()

    numbers = input_str.split(",")

    # sum to 0
    total_sum = 0

    for number in numbers:
        try:
            total_sum += float(number)
        except ValueError:
            print(f"Invalid number: {number}")

    return total_sum


input_str = input("Enter a string containing numbers separated by commas (max 72 characters): ")

total_sum = calculate_sum(input_str)

# Print the user input and sum
print(f"User Input: {input_str}")
print(f"Sum of Numbers: {total_sum}")







def area(x1, y1, x2, y2, x3, y3): #6
    return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
                + x3 * (y1 - y2)) / 2.0)
def isInside(x1, y1, x2, y2, x3, y3, x, y):
    A = area (x1, y1, x2, y2, x3, y3) 
    A1 = area (x, y, x2, y2, x3, y3)     
    A2 = area (x1, y1, x, y, x3, y3)   
    A3 = area (x1, y1, x2, y2, x, y)   
    if(A == A1 + A2 + A3):
        return True
    else:
        return False
a1=int(input("enter x1="))
b1=int(input("enter y1="))
a2=int(input("enter x2="))
b2=int(input("enter y1="))
a3=int(input("enter x3="))
b3=int(input("enter y3="))
n=int(input("n="))
for i in range(n) :
  p1=int(input("enter p1="))
  p2=int(input("enter p2="))
  if (isInside(a1, b1, a2, b2, a3, b3, p1, p2)):
    print('Inside')
  else:
    print('outside')



num_subjects = int(input("Enter the number of subjects: "))
subjects = []
students = []
marks = []

for i in range(num_subjects):
    subject_name = input(f"Enter the name of subject {i+1}: ")
    subjects.append(subject_name)
    student_name = input(f"Enter the name of student who scored highest in {subject_name}: ")
    students.append(student_name)
    mark = int(input(f"Enter the marks scored by {student_name} in {subject_name}: "))
    marks.append(mark)

final_list = []
for i in range(num_subjects):
    subject_dict = {}
    subject_dict[subjects[i]] = {students[i]: marks[i]}
    final_list.append(subject_dict)

print(f"Number of Subjects = {num_subjects}")
print(f"Subjects = {subjects}")
print(f"Students = {students}")
print(f"Marks = {marks}")
print(f"Final List = {final_list}")